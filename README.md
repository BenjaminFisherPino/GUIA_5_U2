# Guia 5 U2

**Versión 1.0**

Archivo README.md para guía numero 5 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 18/10/2021

---
## Resúmen del programa

Este programa fue hecho para el almacenamiento y visualizacion del contenido de un arbol binario balanceado (almacenamiento dinamico).

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0, la version 4.2.1 de make y el paquete graphviz. En caso de no tener el compilador de g++, make o graphviz y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

### Instalacion de graphviz

Para instalar graphviz debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install graphviz"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar grafos del arbol construido.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_5_U2.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++, la version 4.2.1 de make y graphviz instalado.

Puede revisar con "g++ --version" y "make --version" respectivamente.

En el caso de graphviz puede comprobar escribiendo en su terminal "man graphviz", en caso de que no aparezca informacion del paquete debe revisar el tutorial de instalacion denuevo.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

Este programa, desarrollado por Benjamín Fisher, nos permite almacenar ID's de proteinas en un arbol binario balanceado. El código se estructura en base a una funcion Main, que nos permite mantener el orden de compilación y ejecucion.

Dentro del programa, el usuario encontrará 5 opciones: 

1) La primera opcion es "Insertar", la cual nos permite ingresar una ID de proteina por teclado y añadirla al arbol manteniendo el orden de este.

2) La segunda opcion es "Buscar", la cual nos permite recorrer el arbol en busqueda de un valor ingresado por teclado, en caso de encontrarlo se imprime por terminal "La ID SI se encuentra en el árbol", en caso de que no se encuentre dentro de la estrucutra se imprime "La ID NO se encuentra en el árbol".

3) La tercera opcion es "Eliminacion", esta nos permite eliminar un nodo que contenga la informacion entregada por el usuario. Para esto se recorre el arbol en busca de aquel valor, si se encuentra este valor se procede a remover el nodo, en caso de no encontrarse la informacion, se imprime por terminal "La ID NO se encuentra en el árbol".

4) La cuarta opcion es "Grafo", la cual nos permite generar una imagen .png que nos otorga información respecto al orden de las ID y que tan balanceado esta el arbol. Para esta opcion es necesario tener graphviz instalado.

5) La quinta opcion es "Salir", esta nos permite terminar con la ejecución del programa.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca
