#include <iostream>
#include <fstream>

using namespace std;
#define TRUE 1
#define FALSE 0

//Estructura del nodo
struct NODO{
	string info;
	int FE;
	NODO *izq, *der;
};

//Prototipos
void InsercionBalanceado(NODO **nodocabeza, int *BO, string infor);
void Busqueda(NODO *nodo, string infor);
void Restructura1(NODO **nodocabeza, int *BO);
void Restructura2(NODO **nodocabeza, int *BO);
void Borra(NODO **aux1, NODO **otro1, int *BO);
void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor);
int Menu();
void GenerarGrafo(NODO *p);
void PreOrden(NODO *, FILE *fp);

//Opciones del menu
int Menu() {
  int Op;
  
  do {
    cout << "\n--------------------\n";
    cout << "1) Insertar\n";
    cout << "2) Buscar\n";
    cout << "3) Eliminacion\n";
    cout << "4) Grafo\n";
    cout << "0) Salir\n\n";
    cout << "Opción: ";
    cin >> Op;
  } while (Op<0 || Op>7);
  
  return (Op);
}

//Insertar
void InsercionBalanceado(NODO **nodocabeza, int *BO, string infor) {
  NODO *nodo = NULL;
  NODO *nodo1 = NULL;
  NODO *nodo2 = NULL; 
  
  nodo = *nodocabeza;
  
  if (nodo != NULL) {
    
    if (infor < nodo->info) {
      InsercionBalanceado(&(nodo->izq), BO, infor);
      
      if(*BO == TRUE) {
        
        switch (nodo->FE) {
          case 1: 
            nodo->FE = 0;
            *BO = FALSE;
            break;
          
          case 0: 
            nodo->FE = -1;
            break;
     
          case -1: 
            /* reestructuración del árbol */
            nodo1 = nodo->izq;
            
            /* Rotacion II */
            if (nodo1->FE <= 0) { 
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            
            } else { 
              /* Rotacion ID */
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;
         
              if (nodo2->FE == -1)
                nodo->FE = 1;
              else
                nodo->FE =0;
        
              if (nodo2->FE == 1)
                nodo1->FE = -1;
              else
                nodo1->FE = 0;
              
              nodo = nodo2;
            }
            
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      } 
      
    } else {
      
      if (infor > nodo->info) {
        InsercionBalanceado(&(nodo->der), BO, infor);
        
        if (*BO == TRUE) {
          
          switch (nodo->FE) {
            
            case -1: 
              nodo->FE = 0;
              *BO = FALSE;
              break;
            
            case 0: 
              nodo->FE = 1;
              break;
      
            case 1: 
              /* reestructuración del árbol */
              nodo1 = nodo->der;
              
              if (nodo1->FE >= 0) { 
                /* Rotacion DD */
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;
                
              } else { 
                /* Rotacion DI */
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;
                
                if (nodo2->FE == 1)
                  nodo->FE = -1;
                else
                  nodo->FE = 0;
       
                if (nodo2->FE == -1)
                  nodo1->FE = 1;
                else
                  nodo1->FE = 0;
   
                nodo = nodo2;
              }
              
              nodo->FE = 0;
              BO = FALSE;
              break;
          }
        }
      } else {
        cout << "El nodo ya se encuentra en el árbol" << endl;
      }
    }
  } else {
    
    nodo = new NODO();
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
  }
  
  *nodocabeza = nodo;
}

//Busqueda
void Busqueda(NODO *nodo,string infor) {
  if (nodo != NULL) {
    if (infor < nodo->info) {
      Busqueda(nodo->izq,infor);
    } else {
      if (infor > nodo->info) {
        Busqueda(nodo->der,infor);
      } else {
        cout << "El nodo SI se encuentra en el árbol" << endl;
      }
    }
  } else {
    cout << "El nodo NO se encuentra en el árbol" << endl;
  }
}

//REESTRUCTURACION
void Restructura1(NODO **nodocabeza, int *BO) {
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case -1: 
        nodo->FE = 0;
        break;
      
      case 0: 
        nodo->FE = 1;
        *BO = FALSE;
        break;
   
    case 1: 
      /* reestructuracion del árbol */
      nodo1 = nodo->der;
      
      if (nodo1->FE >= 0) { 
        /* rotacion DD */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        
        switch (nodo1->FE) {
          case 0: 
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          case 1: 
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;           
        }
        nodo = nodo1;
      } else { 
        /* Rotacion DI */
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;
       
        if (nodo2->FE == 1)
          nodo->FE = -1;
        else
          nodo->FE = 0;
        
        if (nodo2->FE == -1)
          nodo1->FE = 1;
        else
          nodo1->FE = 0;
       
        nodo = nodo2;
        nodo2->FE = 0;       
      } 
      break;   
    }
  }
  *nodocabeza=nodo;
}

void Restructura2(NODO **nodocabeza, int *BO) {
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case 1: 
        nodo->FE = 0;
        break;
      case 0: 
        nodo->FE = -1;
        *BO = FALSE;
        break;
      case -1: 
        /* reestructuracion del árbol */
        nodo1 = nodo->izq;
        if (nodo1->FE<=0) { 
          /* rotacion II */
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          switch (nodo1->FE) {
            case 0: 
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            case -1: 
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        } else { 
          /* Rotacion ID */
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;
       
          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;
        
          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;
       
          nodo = nodo2;
          nodo2->FE = 0;       
        }      
        break;   
    }
  }
  *nodocabeza = nodo;
}

//ELIMINAR
void Borra(NODO **aux1, NODO **otro1, int *BO) {
  NODO *aux, *otro; 
  aux=*aux1;
  otro=*otro1;
  
  if (aux->der != NULL) {
    Borra(&(aux->der),&otro,BO); 
    Restructura2(&aux,BO);
  } else {
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  *aux1=aux;
  *otro1=otro;
}

void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor) {
  NODO *nodo, *otro; 
  
  nodo = *nodocabeza;
  
  if (nodo != NULL) {
    if (infor < nodo->info) {
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      Restructura1(&nodo,BO);
    } else {
      if (infor > nodo->info) {
        EliminacionBalanceado(&(nodo->der),BO,infor);
        Restructura2(&nodo,BO); 
      } else {
        otro = nodo;
        if (otro->der == NULL) {
          nodo = otro->izq;
          *BO = TRUE;     
        } else {
          if (otro->izq==NULL) {
            nodo=otro->der;
            *BO=TRUE;     
          } else {
            Borra(&(otro->izq),&otro,BO);
            Restructura1(&nodo,BO);
            //remove(otro);
          }
        }
      }
    } 
  } else {
    
    cout << "El nodo NO se encuentra en el arbol" << endl;
  }
  *nodocabeza=nodo;
}

//CALSE GRAFO
class Grafo {
    private:
    
    public:
        Grafo (NODO *nodo) {
            ofstream fp;
            
            /* abre archivo */
            fp.open ("grafo.txt");

            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;
                
            recorrer(nodo, fp);

            fp << "}" << endl;

            /* cierra archivo */
            fp.close();
                    
            /* genera el grafo */
            system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
            system("eog grafo.png &");
        }
        
        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        */
        void recorrer(NODO *arbol, ofstream &fp) {
			if (arbol != NULL){
				if (arbol->izq != NULL){
					fp << "\n" << arbol->info << "->" << 
					arbol->izq->info << " [label="<< arbol->izq->FE << "]"<<";";
				} else {
					fp << "\n" << '"' << arbol->info << "i" << '"' << " [shape=point];";
					fp << "\n" << arbol->info << "->" << '"' << arbol->info << "i" << '"';
				}
				
				if (arbol->der != NULL){
					fp << "\n" << arbol->info << "->" << 
					arbol->der->info << " [label="<< arbol->der->FE << "]"<<";";
				} else{
					fp << "\n" << '"' << arbol->info << "d" << '"' << " [shape=point];";
					fp << "\n" << arbol->info << "->" << '"' << arbol->info << "d" << '"';
				}
				recorrer(arbol->izq, fp);
				recorrer(arbol->der, fp);
			}
		}
};

//FUNCION MAIN
int main(void) {
  int opcion;
  string elemento;
  NODO *raiz = NULL;
 
  opcion = Menu();
  int inicio;
  
  while (opcion) {
    
    switch(opcion) {
      case 1: //Ingresar elementos al arbol
        cout << "Ingresar elemento: ";
        cin >> elemento;
        inicio=FALSE;
        InsercionBalanceado(&raiz, &inicio, elemento);
        break;

      case 2: //Buscar elementos en el arbol
        cout << "Buscar elemento: ";
        cin >> elemento;
        Busqueda(raiz, elemento);
        break;
      
      case 3: //Eliminar un elemento del arbol
        cout << "Eliminar elemento: ";
        cin >> elemento;
        inicio=FALSE;
        EliminacionBalanceado(&raiz, &inicio, elemento);
        break;
      
      case 4: //Generar un grafo del arbol
        Grafo *g = new Grafo(raiz);
        break;
    }
    
    opcion = Menu();
  }
  
  return 0;
}
